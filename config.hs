module Main(main) where

import XMonad
import XMonad.Hooks.ManageHelpers

import qualified Data.Map as M
import XMonad.Prompt.Shell
import XMonad.Prompt
import XMonad.Config.Desktop
import XMonad.Layout.Fullscreen
import XMonad.Layout.NoBorders (smartBorders)

main :: IO ()
main = xmonad $ desktopConfig
       { modMask = mod4Mask
       , manageHook = fullscreenManageHook <+> myManageHook <+> manageHook desktopConfig
       , keys = \c -> myKeys c `M.union` keys desktopConfig c
       , terminal = "urxvt"
       , handleEventHook = handleEventHook desktopConfig <+> fullscreenEventHook
       , layoutHook = smartBorders $ fullscreenFocus $ layoutHook desktopConfig
       }

myManageHook :: ManageHook
myManageHook = composeOne
  [ transience
  , isDialog -?> doCenterFloat
  ]

myKeys :: XConfig l -> M.Map (KeyMask, KeySym) (X ())
myKeys XConfig {modMask = modm} = M.fromList
  [ ((modm, xK_x), shellPrompt myPromptConfig)
  , ((modm, xK_q), spawn recompileCommand)
  ]
  where
    recompileCommand :: String
    recompileCommand = concat
      [ "xmonad --recompile && "
      , "xmonad --restart && "
      , "notify-send -t 1000 'XMonad successfully recompiled!'"
      ]

myPromptConfig :: XPConfig
myPromptConfig = def
  { font = "xft:IBM Plex Mono:size=9"
  , position = CenteredAt (1/2) (1/2)
  , height = 32
  }
